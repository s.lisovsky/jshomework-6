let arr = [1,2,3,'3','3','3'];

function filterBy(array, typeData){
    return array.filter(function (item) {
        return typeof(item) === typeData;
    });
}

let res = filterBy(arr, 'string');
console.log(res);